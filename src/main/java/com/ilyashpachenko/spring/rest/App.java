package com.ilyashpachenko.spring.rest;

import com.ilyashpachenko.spring.rest.configuration.MyConfig;
import com.ilyashpachenko.spring.rest.entity.Employee;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(MyConfig.class);
        Communication communication = context.getBean("communication",
                Communication.class);

        List<Employee> allEmployees = communication.getAllEmployees();
        System.out.println(allEmployees);

        Employee empById = communication.getEmployee(2);
        System.out.println(empById);

        Employee employee = new Employee("Volodya", "Eygenson", "HR", 1500);
        communication.saveEmployee(employee);

        Employee emp = new Employee("Volodya", "Eygenson", "IT", 2700);
        emp.setId(10);
        communication.saveEmployee(emp);

        communication.deleteEmployee(11);
    }
}
